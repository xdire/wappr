package io.xdire.wappr.http;

import java.util.HashMap;
import java.util.Map;

public class HttpPackage {

    private String method;

    private String host;

    private String connection;

    private String upgradeType;

    private String websocketKey;

    private String websocketExt;

    private Map<String,String> headers = new HashMap<>();

    public boolean parseHeaders(char[] a) {

        //System.out.println("START PARSING HEADERS");
        //System.out.println("\n ---------------------------------------- \n");

        char buffer[] = new char[512];
        int counter = 0;

        String header = "";
        String value = "";

        boolean isValue = false;

        System.out.println(a);

        for(char c : a) {

            if(c == '\r') {
                continue;
            }
            if(c == '\n') {

                //System.out.println("\n---------------- NEW LINE -------------\n");

                if(isValue) {

                    isValue = false;
                    value = bufferToString(buffer,counter);

                    analyzeHeaderData(header,value);

                    header = "";

                }

                buffer = new char[512];
                counter = 0;
                continue;

            }

            if(c == ':') {

                if(!isValue) {

                    isValue = true;
                    header = bufferToString(buffer,counter);

                    buffer = new char[512];
                    counter = 0;
                    continue;
                }

            }

            if(counter < 512) {
                buffer[counter++] = c;
            }
            else {
                isValue = false;
                value = bufferToString(buffer,counter);
                analyzeHeaderData(header,value);
                header = "";
                buffer = new char[512];
                counter = 0;
            }

        }

        return true;

    }

    private void analyzeHeaderData(String header, String value) {

        String lch= header.toLowerCase();

        switch (lch) {

            case "upgrade":
                this.upgradeType = value.trim();
                break;

            case "connection":
                this.connection = value.trim();
                break;

            case "sec-websocket-key":
                this.websocketKey = value.trim();
                break;

            case "sec-websocket-extensions":
                this.websocketExt = value.trim();
                break;

            default:
                this.headers.put(header,value.trim());
                break;

        }

    }

    private String bufferToString(char[] buffer, int stop) {
        return new String(buffer,0,stop);
    }

    public String getConnection() {
        return connection;
    }

    public String getHeader(String header) {
        return headers.get(header);
    }

    public String getHost() {
        return host;
    }

    public String getMethod() {
        return method;
    }

    public String getUpgradeType() {
        return upgradeType;
    }

    public String getWebsocketExt() {
        return websocketExt;
    }

    public String getWebsocketKey() {
        return websocketKey;
    }

}

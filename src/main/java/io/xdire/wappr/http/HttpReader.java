package io.xdire.wappr.http;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by xdire on 7/7/16.
 */
public class HttpReader {

    public HttpPackage readFromStream(InputStream in) {

        HttpPackage headers = new HttpPackage();

        try {

            char[] b = new char[1024];
            int read = 0;

            int chunk;

            byte rnseqchr = 0;
            byte prev = ' ';

            // -----------------------------------------
            //          READ INCOMING HEADERS
            // -----------------------------------------

            while ((chunk = in.read()) != -1) {

                b[read++] = (char) chunk;

                // Read headers end sequence
                // Increase on \n (LF) \r (CR)
                if(chunk == 10 || chunk == 13) {
                    if(prev == 10 || prev == 13) {
                        rnseqchr++;
                    }
                } else rnseqchr = 0;


                // If end sequence is satisfied then
                // continue execution
                if(rnseqchr == 3) break;

                // Hold previous character as guaranteed
                // marker of previous symbol
                prev = (byte)chunk;

            }

            // ------------------------------------------
            headers.parseHeaders(reduceBuffer(b,read));

        } catch (IOException e) {
            e.printStackTrace();
        }

        return headers;

    }

    private char[] reduceBuffer(char[] c, int limit){

        char[] r = new char[limit];

        for (int i = 0; i < limit; i++ ){

            r[i] = c[i];

        }

        return r;
    }

}

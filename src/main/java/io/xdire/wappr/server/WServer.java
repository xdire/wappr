package io.xdire.wappr.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by xdire on 7/7/16.
 */
public class WServer implements Runnable {

    private final int port;
    private final String addr;

    private ServerSocket server;
    private Socket client;

    private final int connLimit;
    private ExecutorService runner;

    private boolean running = false;
    private int status = 0;

    public WServer(String addr, int port, int connectionLimit) {
        this.addr = addr;
        this.port = port;
        this.connLimit = connectionLimit;
        this.initPool();
    }

    public WServer(int port, int connectionLimit) {
        this.port = port;
        this.addr = "";
        this.connLimit = connectionLimit;
        this.initPool();
    }

    private void initPool() {
        this.runner = Executors.newFixedThreadPool(this.connLimit);
    }

    @Override
    public void run() {
        this.start();
    }

    public void start() {

        if(this.addr.length() == 0) {

            try {

                this.server = new ServerSocket(this.port);
                this.running = true;

                System.out.println("WServer started and accepting connections: " + this.port);

                while (running) {

                    try {

                        Socket client = server.accept();


                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }

            } catch (IOException e) {
                e.printStackTrace();
            }

        }

    }

}

package io.xdire.wappr.websocket;

import io.xdire.wappr.protocol.InteractionPackage;

public class WebsocketPackage implements InteractionPackage{

    @Override
    public byte[] create(String string) {
        return new byte[0];
    }

    @Override
    public void fromBytes() {

    }

    @Override
    public int length() {
        return 0;
    }
}

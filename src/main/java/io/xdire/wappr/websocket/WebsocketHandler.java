package io.xdire.wappr.websocket;

import io.xdire.wappr.protocol.CommandReader;
import io.xdire.wappr.protocol.WrappableSocket;

import java.io.InputStreamReader;

public class WebsocketHandler {

    private final WrappableSocket client;

    private final InputStreamReader reader;

    private int connectionStatus = 0;

    public WebsocketHandler(WrappableSocket client, CommandReader commander) {

        this.client = client;
        reader = new InputStreamReader(client.getInputStream());

    }

    /** ------------------------------------------------------------------------
     *
     *
     *  ------------------------------------------------------------------------
     */
    public void send() {

    }

    /** ------------------------------------------------------------------------
     *
     *
     *  ------------------------------------------------------------------------
     */
    public void read() {

    }

    /** ------------------------------------------------------------------------
     *
     *
     *  ------------------------------------------------------------------------
     */
    public void drop() {

    }

    /** ------------------------------------------------------------------------
     *
     *
     *  ------------------------------------------------------------------------
     * @return
     */
    public boolean isConnected() {
        return this.connectionStatus == 3;
    }


}

package io.xdire.wappr.protocol;

/**
 * Created by xdire on 7/7/16.
 */
public interface CommandReader {

    public void decode(String data);

    public void fromPackage(InteractionPackage interactionPackage);

}

package io.xdire.wappr.protocol;

/**
 * Created by xdire on 7/7/16.
 */
public interface InteractionPackage {

    public byte[] create(String string);

    public void fromBytes();

    public int length();

}

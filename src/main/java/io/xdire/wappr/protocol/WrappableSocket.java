package io.xdire.wappr.protocol;

import java.io.InputStream;
import java.io.OutputStream;

public interface WrappableSocket {

    public OutputStream getOutputStream();

    public InputStream getInputStream();

    public void stop();

}

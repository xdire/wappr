package io.xdire.wappr.client;

import io.xdire.wappr.command.PrintCommandReader;
import io.xdire.wappr.http.HttpPackage;
import io.xdire.wappr.http.HttpReader;
import io.xdire.wappr.protocol.WrappableSocket;
import io.xdire.wappr.websocket.WebsocketHandler;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

/**
 * Created by xdire on 7/7/16.
 */
public class WClient implements Runnable, WrappableSocket {

    private Socket client;

    private boolean running = true;

    private InputStream is;
    private OutputStream os;

    public WClient(Socket client) throws IOException {

        this.client = client;
        this.is = client.getInputStream();
        this.os = client.getOutputStream();

        PrintCommandReader reader = new PrintCommandReader();
        WebsocketHandler ws = new WebsocketHandler(this, reader);
        HttpReader httpReader = new HttpReader();

        while(true) {

            if(ws.isConnected()) {

                ws.read();

            } else {

                HttpPackage pack = httpReader.readFromStream(is);

            }

            try {
                Thread.sleep(30);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }

    }

    @Override
    public void run() {

    }

    @Override
    public InputStream getInputStream() {
        return is;
    }

    @Override
    public OutputStream getOutputStream() {
        return os;
    }

    @Override
    public void stop() {
        this.running = false;
    }

}
